from django.urls import path
from todos.views import (
    AllTodosList,
    CurrentTodoList,
    CreateTodoList,
    EditTodoList,
    DeleteTodoList,
    CreateTodoItem,
    EditTodoItem,
)

urlpatterns = [
    path("", AllTodosList, name="todo_list_list"),
    path("<int:id>/", CurrentTodoList, name="todo_list_detail"),
    path("create/", CreateTodoList, name="todo_list_create"),
    path("<int:id>/edit/", EditTodoList, name="todo_list_update"),
    path("<int:id>/delete/", DeleteTodoList, name="todo_list_delete"),
    path("items/create/", CreateTodoItem, name="todo_item_create"),
    path("items/<int:id>/edit/", EditTodoItem, name="todo_item_update"),
]
