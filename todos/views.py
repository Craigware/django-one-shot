from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


def AllTodosList(request):
    all_lists = TodoList.objects.all()

    context = {
        "all_lists": all_lists,
    }

    return render(request, "todo_lists.html", context)


def CurrentTodoList(request, id):
    current_list = TodoList.objects.get(id=id)

    context = {
        "current_list": current_list,
    }

    return render(request, "todo_list_detail.html", context)


def CreateTodoList(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)

    else:
        form = TodoForm()

    context = {"form": form}
    return render(request, "todo_list_create.html", context)


def EditTodoList(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)

    else:
        form = TodoForm(instance=model_instance)

    context = {"form": form}

    return render(request, "todo_list_edit.html", context)


def DeleteTodoList(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todo_list_delete.html")


def CreateTodoItem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", form.cleaned_data["list"].id)

    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todo_task_create.html", context)


def EditTodoItem(request, id):
    model_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=form.cleaned_data["list"].id)

    else:
        form = TodoItemForm(instance=model_instance)

    context = {"form": form}

    return render(request, "todo_item_update.html", context)
